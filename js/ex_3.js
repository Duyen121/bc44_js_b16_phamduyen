function calculateFactorial() {
  var n = document.querySelector("#number-n-2").value * 1;
  var factorial = 1;
  for (let i = 1; i <= n; i++) {
    factorial *= i;
  }
  document.getElementById("factorial").innerText = `${factorial}`;
}
