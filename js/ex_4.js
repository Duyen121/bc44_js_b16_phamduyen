function createDivTag() {
  for (let i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      var oddNumDiv = document.createElement("div");
      var oddNumDivText = document.createTextNode(`Div lẻ ${i}`);
      oddNumDiv.appendChild(oddNumDivText);
      document.querySelector(".ex-4-result").appendChild(oddNumDiv);
      oddNumDiv.style.backgroundColor = "#408E91";
    } else {
      var evenNumDiv = document.createElement("div");
      var evenNumDivText = document.createTextNode(`Div chẵn ${i}`);
      evenNumDiv.appendChild(evenNumDivText);
      document.querySelector(".ex-4-result").appendChild(evenNumDiv);
      evenNumDiv.style.backgroundColor = "#E49393";
    }
  }
}
