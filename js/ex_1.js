var sum = 0;
var n = 0;

do {
  n++;
  sum += n;
} while (sum < 10000);

document.getElementById(
  "final-result"
).innerText = `Số nguyên dương nhỏ nhất: ${n}`;
